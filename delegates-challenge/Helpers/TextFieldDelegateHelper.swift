//
//  TextFieldDelegateHelper.swift
//  delegates-challenge
//
//  Created by  Ahmed Shendy on 9/7/18.
//  Copyright © 2018 Ahmed Shendy. All rights reserved.
//

import Foundation
import UIKit

class TextFieldDelegateHelper {
    
    //MARK: Properties
    
    let zipcodeLength = 5
    let dollarSignPosition = String.Index.init(encodedOffset: )
    
    //MARK: Helper Methods
    
    public func buildNewText(from oldText: String, in range: NSRange, with newCharacter: String) -> String {
        let newText = oldText as NSString
        return newText.replacingCharacters(in: range, with: newCharacter)
    }
    
    public func isValidZipcodeLength(_ zipcode: String) -> Bool {
        return zipcode.count <= zipcodeLength && Int(zipcode) != nil
    }
    
    public func isValidCachNumber(cashString: String) -> Bool {
        let number
        removeDollarSign(cashString)
        
        fixFloatingPoint(cashString)
        
        return
        
        if Double(cashString) == nil {
            return false
        }
        
        
    }
    
    public func replacementStringIsEmpty(_ string: String) -> Bool {
        return string == ""
    }
}
