//
//  ZipcodeTextFieldDelegate.swift
//  delegates-challenge
//
//  Created by  Ahmed Shendy on 9/7/18.
//  Copyright © 2018 Ahmed Shendy. All rights reserved.
//

import Foundation
import UIKit

class ZipcodeTextFieldDelegate: NSObject, UITextFieldDelegate {
    
    //MARK: Properties
    
    let zipcodeLength = 5
    
    //MARK: Text Field Delegate Methods
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let newText = buildNewText(from: textField.text!, in: range, with: string)
        return isValidZipcodeLength(newText) || replacementStringIsEmpty(string)
    }
    
    //MARK: Text Field Delegate Helpers
    
    private func buildNewText(from oldText: String, in range: NSRange, with newCharacter: String) -> String {
        let newText = oldText as NSString
        return newText.replacingCharacters(in: range, with: newCharacter)
    }
    
    private func isValidZipcodeLength(_ zipcode: String) -> Bool {
        return zipcode.count <= zipcodeLength && Int(zipcode) != nil
    }
    
    private func replacementStringIsEmpty(_ string: String) -> Bool {
        return string == ""
    }
}
