//
//  CashTextFieldDelegate.swift
//  delegates-challenge
//
//  Created by  Ahmed Shendy on 9/7/18.
//  Copyright © 2018 Ahmed Shendy. All rights reserved.
//

import Foundation
import UIKit

class CashTextFieldDelegate: NSObject, UITextFieldDelegate {
    
    //MARK: Properties
    
    let dollarSign = "$"
    var defaultCashString = "0.00"
    let dollarSignIndex = String.Index.init(encodedOffset: 0)
    
    //MARK: Enums
    
    enum FloatingPointShiftDirection {
        case left
        case right
    }
    
    //MARK: Text Field Delegate Methods
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if userIsAddingOrRemovingNumber(newCharacter: string) {
            let newCashString = buildNewCachString(from: textField.text!, in: range, with: string)
            textField.text = newCashString
        }
        
        return false
    }
    
    //MARK: Text Field Delegate Helpers
    
    private func buildNewCachString(from oldCashString: String, in range: NSRange, with newCharacter: String) -> String {
        var newCashString = (oldCashString as NSString).replacingCharacters(in: range, with: newCharacter)

        newCashString.removeFirst() // This would remove 
        newCashString = shiftFloatingPoint(of: newCashString, to: newCharacter == "" ? .left : .right)
        
        return prependDollarSignToCashString(newCashString)
    }
    
    private func prependDollarSignToCashString(_ cashString: String) -> String {
        return Float(cashString) == 0 ? "\(dollarSign)\(defaultCashString)" : "\(dollarSign)\(cashString)"
    }
    
    private func userIsAddingOrRemovingNumber(newCharacter: String) -> Bool {
      return newCharacter == "" || Int(newCharacter) != nil
    }
    
    private func shiftFloatingPoint(of cashString: String, to direction: FloatingPointShiftDirection) -> String {
        return direction == .left ?
              String( Double(cashString)! / 10.0 )
            : String( Double(cashString)! * 10.0 )
    }
}
