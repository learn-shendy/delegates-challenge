//
//  DelegatesChallengeViewController.swift
//  udacity-delegates-app
//
//  Created by  Ahmed Shendy on 9/7/18.
//  Copyright © 2018 Ahmed Shendy. All rights reserved.
//

import UIKit

class DelegatesChallengeViewController: UIViewController {
    
    //MARK: Outlet Properties
    
    @IBOutlet weak var zipcodeTextField: UITextField!
    @IBOutlet weak var cashTextField: UITextField!
    @IBOutlet weak var lockableTextField: UITextField!
    @IBOutlet weak var locakableSwitch: UISwitch!
    
    
    //MARK: Delegates Properties
    
    let zipcodeTextFieldDelegate = ZipcodeTextFieldDelegate()
    let cashTextFieldDelegate = CashTextFieldDelegate()

    //MARK: App Lifecycle Methods
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        assignTextFieldsDelegates()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        setCashTextFieldToDefaults()
        setZipcodeTextFieldToDefaults()
        setlockableTextFieldToDefaults()
    }
    
    //MARK: Action Methods
    
    @IBAction func switchLockableTextFieldStatus(_ sender: UISwitch) {
        lockableTextField.isEnabled = sender.isOn ? true : false
    }
    
    //MARK: Private Methods
    
    private func assignTextFieldsDelegates() {
        zipcodeTextField.delegate = zipcodeTextFieldDelegate
        cashTextField.delegate = cashTextFieldDelegate
    }
    
    private func setCashTextFieldToDefaults() {
        cashTextField.text = "$0.00"
        cashTextField.placeholder = "Type your cash here (e.g: $1234.45)"
        cashTextField.keyboardType = .numberPad
    }
    
    private func setZipcodeTextFieldToDefaults() {
        zipcodeTextField.text = ""
        zipcodeTextField.placeholder = "Type your zip code here (e.g: 11728)"
        zipcodeTextField.textContentType = .postalCode
        zipcodeTextField.keyboardType = .numberPad
    }
    
    private func setlockableTextFieldToDefaults() {
        lockableTextField.text = ""
        lockableTextField.placeholder = "Type anything here"
        lockableTextField.isEnabled = false
        
        locakableSwitch.setOn(false, animated: true)
    }
}

